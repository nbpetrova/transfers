package ru.bank.transfers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class Bank {
    private static final Logger logger = LoggerFactory.getLogger(Bank.class);
    private static final Bank instance = new Bank();
    private final Map<Integer, Account> accounts = new HashMap<>();
    private int idGenerator = 0;

    private Bank(){
    }

    public static Bank getInstance() {
        return instance;
    }

    public synchronized Account createNewAccount() {
        int id = ++idGenerator;
        Account account = new Account(id);
        accounts.put(id, account);

        logger.trace("A new account was created: {}", id);
        return account;
    }

    public synchronized Account getAccountInfo(int id) {
        return accounts.get(id);
    }

    public synchronized Account getAccountInfo(String id) {
        try {
            int accountId = Integer.parseInt(id);
            return accounts.get(accountId);
        } catch (Exception e) {
            logger.error("Cannot parse Integer from String: {}", id);
            return null;
        }
    }
}
