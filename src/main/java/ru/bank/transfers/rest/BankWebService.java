package ru.bank.transfers.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.bank.transfers.Account;
import ru.bank.transfers.Bank;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.text.MessageFormat;

@Path("/api")
public class BankWebService {
    private static final Logger logger = LoggerFactory.getLogger(BankWebService.class);
    private Bank bank = Bank.getInstance();

    @POST
    @Path("account")
    @Produces(MediaType.TEXT_PLAIN)
    public Response createNewAccount() {
        Account account = bank.createNewAccount();

        return Response.ok(account.getId()).build();
    }

    @GET
    @Path("account/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getCurrentBalance(@PathParam("id") String id) {
        Account account = bank.getAccountInfo(id);
        if(account == null) {
            return accountNotFound(id);
        }

        return Response.ok(account.getBalance()).build();
    }

    @PUT
    @Path("account/add/{to}/{amount}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response add(@PathParam("to") String id, @PathParam("amount") String amountParam) {
        BigDecimal amount = toBigDecimal(amountParam);
        if(amount.signum() < 0) {
            return illegalAmount();
        }

        Account to = bank.getAccountInfo(id);
        if(to == null) {
            return accountNotFound(id);
        }

        try {
            to.addMoney(amount);
        } catch (Exception e) {
            logger.error("Error occurred while adding money to account {}: ", id, e);
            return Response.serverError().entity("Cannot perform the operation: " + e.getMessage()).build();
        }

        return Response.ok("Ok").build();
    }

    @POST
    @Path("transfer/{from}/{to}/{amount}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response transfer(@PathParam("from") String fromId, @PathParam("to") String toId,
            @PathParam("amount") String amountParam) {
        BigDecimal amount = toBigDecimal(amountParam);
        if(amount.signum() < 0) {
            return illegalAmount();
        }

        Account from = bank.getAccountInfo(fromId);
        if(from == null) {
            return accountNotFound(fromId);
        }
        Account to = bank.getAccountInfo(toId);
        if(to == null) {
            return accountNotFound(toId);
        }

        try {
            from.transferMoney(to, amount);
        } catch (Exception e) {
            logger.error("Error occurred while transferring from {} to {}: ", fromId, toId, e);
            return Response.serverError().entity("Cannot perform the operation: " + e.getMessage()).build();
        }

        return Response.ok("Ok").build();
    }

    @GET
    @Path("/available")
    @Produces(MediaType.TEXT_PLAIN)
    public String available() {
        return "yes";
    }

    private Response illegalAmount() {
        return Response.status(Response.Status.BAD_REQUEST).entity("Amount have to be a positive number").build();
    }

    private Response accountNotFound(String id) {
        return Response.status(Response.Status.NOT_FOUND).entity(
                MessageFormat.format("Account not found for id: {0}", id)).build();
    }

    private BigDecimal toBigDecimal(String s) {
        try {
            double d = Double.parseDouble(s);
            return BigDecimal.valueOf(d);
        } catch (Exception e) {
            logger.error("Cannot parse BigDecimal from String: {}", s);
            return BigDecimal.valueOf(-1);
        }
    }
}
