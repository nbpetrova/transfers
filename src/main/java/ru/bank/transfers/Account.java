package ru.bank.transfers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.bank.transfers.exception.BankException;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Account {
    private static final Logger logger = LoggerFactory.getLogger(Account.class);

    private final int id;
    private BigDecimal balance = BigDecimal.ZERO;
    private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private Lock readLock = lock.readLock();
    private Lock writeLock = lock.writeLock();

    public Account(int id) {
        this.id = id;
    }

    public void addMoney(BigDecimal amount) throws BankException {
        validate(amount);

        writeLock.lock();
        try {
            balance = balance.add(amount);
            logger.debug("{} was added, current balance: {}" ,amount, balance);
        } finally {
            writeLock.unlock();
        }
    }

    public void takeMoney(BigDecimal amount) throws BankException {
        validate(amount);

        writeLock.lock();
        try {
            if (balance.compareTo(amount) < 0) {
                throw new BankException("Not enough money to complete the transaction");
            }
            balance =  balance.subtract(amount);
            logger.debug("{} was taken, current balance: {}" ,amount, balance);
        } finally {
            writeLock.unlock();
        }
    }

    public int getId() {
        return id;
    }

    public BigDecimal getBalance() {
        readLock.lock();
        try {
            return balance;
        } finally {
            readLock.unlock();
        }
    }

    public void transferMoney(Account to, BigDecimal amount) throws BankException {
        if (to == null) {
            throw new BankException("Invalid account");
        }

        if (this.equals(to)) {
            return;
        }

        while (true) {
            if (this.writeLock.tryLock()) {
                if (to.writeLock.tryLock()) {
                    try {
                        logger.debug("Before transferring: from balance {}, to balance {}",
                                this.getBalance(), to.getBalance());
                        this.takeMoney(amount);
                        to.addMoney(amount);
                        logger.debug("After transferring: from balance {}, to balance {}",
                                this.getBalance(), to.getBalance());
                    } finally {
                        to.writeLock.unlock();
                        this.writeLock.unlock();
                    }

                    return;

                } else {
                    this.writeLock.unlock();
                }
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new BankException("Cannot perform transfer: the thread was interrupted");
            }
        }
    }

    private void validate(BigDecimal amount) throws BankException {
        if (amount == null || amount.signum() < 0) {
            throw new BankException("Amount must have positive value");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override public String toString() {
        return "Account{" + "id=" + id + ", balance=" + balance + '}';
    }
}
