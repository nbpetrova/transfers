package ru.bank.transfers;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class BankTest {
    private Bank bank = Bank.getInstance();

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testCreateNewAccount() {
        Account account = bank.createNewAccount();
        assertNotNull(account);
        assertTrue(account.getId() > 0);
        assertEquals(BigDecimal.ZERO, account.getBalance());
    }

    @Test
    public void testGettingAccountInfo() {
        Account account = bank.createNewAccount();
        Account accountInfo = bank.getAccountInfo(account.getId());
        assertEquals(account, accountInfo);
    }

    @Test
    public void testGettingAccountInfoFromStr() {
        Account source = bank.createNewAccount();
        Account fromInt = bank.getAccountInfo(source.getId());
        assertEquals(source, fromInt);

        String idAsStr = String.valueOf(source.getId());
        Account fromStr = bank.getAccountInfo(idAsStr);
        assertEquals(source, fromStr);

        assertNull(bank.getAccountInfo("str"));
        assertNull(bank.getAccountInfo("-1"));
    }
}
