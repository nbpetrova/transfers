package ru.bank.transfers;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.bank.transfers.exception.BankException;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class AccountTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testAddValidAmountOfMoney() throws BankException {
        Account account = new Account(1);
        account.addMoney(BigDecimal.TEN);
        assertEquals(BigDecimal.TEN, account.getBalance());
    }

    @Test
    public void testAddNegativeAmount() throws BankException {
        Account account = new Account(1);
        exception.expect(BankException.class);
        account.addMoney(BigDecimal.valueOf(-1));
    }

    @Test
    public void testSendMoney() throws BankException {
        Account account = new Account(1);
        account.addMoney(BigDecimal.valueOf(100));
        account.takeMoney(BigDecimal.TEN);
        assertEquals(BigDecimal.valueOf(90), account.getBalance());
    }

    @Test
    public void testTakeNegativeAmount() throws BankException {
        Account account = new Account(1);
        exception.expect(BankException.class);
        account.takeMoney(BigDecimal.valueOf(-1));
    }

    @Test
    public void testOverdraft() throws BankException {
        Account account = new Account(1);
        account.addMoney(BigDecimal.TEN);
        exception.expect(BankException.class);
        account.takeMoney(BigDecimal.valueOf(1000));
    }

    @Test
    public void testEquality() throws BankException {
        Account account1 = new Account(1);
        account1.addMoney(BigDecimal.TEN);
        Account account2 = new Account(1);
        account2.addMoney(BigDecimal.TEN);
        assertEquals(account1, account2);

        Account account3 = new Account(3);
        account3.addMoney(BigDecimal.TEN);
        assertNotEquals(account1, account3);

        Account account4 = new Account(1);
        account4.addMoney(BigDecimal.ONE);
        assertEquals(account1, account4);
    }

    @Test
    public void testAddNullValues() throws BankException {
        Account account = new Account(1);
        exception.expect(BankException.class);
        account.addMoney(null);
    }

    @Test
    public void testTakeNullValues() throws BankException {
        Account account = new Account(1);
        exception.expect(BankException.class);
        account.takeMoney(null);
    }

    @Test
    public void testValidTransfer() throws BankException {
        Account account1 = new Account(1);
        Account account2 = new Account(2);
        assertNotEquals(account1, account2);

        account1.addMoney(BigDecimal.TEN);

        account1.transferMoney(account2, BigDecimal.TEN);
        assertEquals(BigDecimal.ZERO, account1.getBalance());
        assertEquals(BigDecimal.TEN, account2.getBalance());
    }

    @Test
    public void testTransferUnaffordableSum() throws BankException {
        Account account1 = new Account(1);
        Account account2 = new Account(2);
        assertNotEquals(account1, account2);

        exception.expect(BankException.class);
        account1.transferMoney(account2, BigDecimal.TEN);
    }

    @Test
    public void testTransferBetweenTheSameAccount() throws BankException {
        Account account = new Account(1);
        account.addMoney(BigDecimal.TEN);

        account.transferMoney(account, BigDecimal.TEN);
        assertEquals(BigDecimal.TEN, account.getBalance());
    }

}
