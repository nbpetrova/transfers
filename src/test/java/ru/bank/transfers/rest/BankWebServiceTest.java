package ru.bank.transfers.rest;

import org.junit.Test;

import javax.ws.rs.core.Response;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BankWebServiceTest {

    private BankWebService service = new BankWebService();

    @Test
    public void testCreateAccount() {
        Response response = service.createNewAccount();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        int idFirst = (Integer) response.getEntity();
        assertTrue(idFirst > 0);
    }

    @Test
    public void testGetCurrentBalance() {
        String id = createAccountAndGetId();

        Response response = service.getCurrentBalance(id);
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        BigDecimal balance = (BigDecimal) response.getEntity();
        assertEquals(BigDecimal.ZERO, balance);
    }

    @Test
    public void testGetNonExistentAccountBalance() {
        Response response = service.getCurrentBalance("-10");
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());

        response = service.getCurrentBalance("100500");
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());

        response = service.getCurrentBalance("one");
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void testAddMoney() {
        String id = createAccountAndGetId();
        BigDecimal currentBalance = getCurrentBalance(id);
        assertEquals(BigDecimal.ZERO, currentBalance);

        Response addResponse = service.add(id, "10.236");
        assertEquals(Response.Status.OK.getStatusCode(), addResponse.getStatus());
        assertEquals(BigDecimal.valueOf(10.236), getCurrentBalance(id));
    }

    @Test
    public void testAddWrongAmount() {
        String id = createAccountAndGetId();

        Response addResponse = service.add(id, "-1.3");
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), addResponse.getStatus());
    }

    @Test
    public void testAddToNonExistentAccount() {
        Response addResponse = service.add("-1", "1.3");
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), addResponse.getStatus());
    }

    @Test
    public void testTransfer() {
        String first = createAccountAndGetId();
        String second = createAccountAndGetId();

        service.add(first, "10.5");

        Response transferResponse = service.transfer(first, second, "10");
        assertEquals(Response.Status.OK.getStatusCode(), transferResponse.getStatus());

        BigDecimal firstBalance = getCurrentBalance(first);
        assertEquals(BigDecimal.valueOf(0.5), firstBalance);

        BigDecimal secondBalance = getCurrentBalance(second);
        assertEquals(BigDecimal.valueOf(10.0), secondBalance);
    }

    @Test
    public void testTransferZeroAmount() {
        String first = createAccountAndGetId();
        String second = createAccountAndGetId();

        Response transferResponse = service.transfer(first, second, "0");
        assertEquals(Response.Status.OK.getStatusCode(), transferResponse.getStatus());

        BigDecimal firstBalance = getCurrentBalance(first);
        assertEquals(BigDecimal.valueOf(0.0), firstBalance);

        BigDecimal secondBalance = getCurrentBalance(second);
        assertEquals(BigDecimal.valueOf(0.0), secondBalance);
    }

    @Test
    public void testTransferWrongAmount() {
        String first = createAccountAndGetId();
        String second = createAccountAndGetId();

        Response transferResponse = service.transfer(first, second, "-20");
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), transferResponse.getStatus());
    }

    @Test
    public void testTransferBetweenNonExistentAccounts() {
        String account = createAccountAndGetId();

        Response transferResponse = service.transfer(account, "-1", "20");
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), transferResponse.getStatus());

        transferResponse = service.transfer("-1", account, "20");
        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), transferResponse.getStatus());
    }


    private BigDecimal getCurrentBalance(String id) {
        Response response = service.getCurrentBalance(id);
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        return  (BigDecimal) response.getEntity();
    }

    private String createAccountAndGetId() {
        Response response = service.createNewAccount();
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        return String.valueOf(response.getEntity());
    }
}
